Categories:
  - Internet
License: GPL-3.0-only
AuthorName: Marcel Bokhorst (M66B)
WebSite: https://email.faircode.eu/
SourceCode: https://github.com/M66B/open-source-email
IssueTracker: https://github.com/M66B/open-source-email/issues
Changelog: https://github.com/M66B/open-source-email/releases
Donate: https://email.faircode.eu/pro/
Bitcoin: 13nUbfsLUzK9Sr7ZJgDRHNR91BJMuDuJnf

AutoName: FairEmail
Summary: Privacy friendly email app
Description: |-
    This email app might be for you if your current email app:
    * takes long to receive or show messages
    * can manage only one mailbox
    * cannot show related messages
    * cannot work offline
    * looks outdated
    * is not maintained
    * stores your email on their servers
    * is closed source, potentially violating your privacy

    '''Features'''
    * 100 % open source
    * Multiple accounts (inboxes)
    * Multiple identities (outboxes)
    * Unified inbox
    * Distraction free reading and writing
    * Message threading
    * Two way synchronization
    * Offline storage and operations
    * Material design
    * Dark theme

    '''Pro features'''
    * Preview sender/subject in new messages status bar notification
    * Encrypt/decrypt messages using OpenPGP via [[org.sufficientlysecure.keychain]]
    * Search on server
    * Standard answers

    '''Simple'''
    * Easy navigation
    * No settings
    * No bells and whistles

    '''Secure'''
    * Allow encrypted connections only
    * Accept valid security certificates only
    * SMTP authentication required
    * Text view only (converted HTML)
    * No special permissions required
    * No advertisements
    * No analytics and no tracking

    '''Efficient'''
    * IMAP IDLE supported
    * Built with latest development tools and libraries
    * Android 6 Marshmallow or later required

    This app starts a foreground service with a low priority status bar
    notification to make sure you'll never miss new email.

RepoType: git
Repo: https://github.com/M66B/open-source-email

Builds:
  - versionName: '0.32'
    versionCode: 32
    commit: '0.32'
    subdir: app
    gradle:
      - yes

  - versionName: '0.36'
    versionCode: 36
    commit: '0.36'
    subdir: app
    gradle:
      - yes

  - versionName: '0.47'
    versionCode: 47
    commit: '0.47'
    subdir: app
    gradle:
      - yes

  - versionName: '0.48'
    versionCode: 48
    commit: '0.48'
    subdir: app
    gradle:
      - yes

  - versionName: '0.54'
    versionCode: 54
    commit: '0.54'
    subdir: app
    gradle:
      - yes

  - versionName: '0.55'
    versionCode: 55
    commit: '0.55'
    subdir: app
    gradle:
      - yes

AutoUpdateMode: Version %v
UpdateCheckMode: Tags
CurrentVersion: '0.55'
CurrentVersionCode: 55
